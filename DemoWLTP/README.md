# DemoWLTP Readme

* DemoWLTP.json export from UCD App
* json files for udclient runs
    * runDeploy_0.0.7.json deploys given version of HelloWLTP
    * uninstall.json removes app from namespace
    * runProcess.json deploys latest version

~~~
udclient -authtoken 8d611514-1abc-43a8-bc19-a97adec143d9 -weburl https://u2004d2.local.net:8443 requestApplicationProcess runDeploy_0.0.7.json

udclient -authtoken 8d611514-1abc-43a8-bc19-a97adec143d9 -weburl https://u2004d2.local.net:8443 requestApplicationProcess uninstall.json 
~~~

