# Templates

## Components

### Componenttemplate for Openshift deployments OCPCOMPONENT

* Uses following component level properties
  * ***AppName***
    * Identifies deployed component with this appname -> this should be also used as replacement token in the yaml files!

* Uses following environment variables for openshift steps!
  * ***Cluster-Token***
  * ***Cluster-URL***
  * ***Target-Namespace***
  * 
  ~~~
  ${p:environment/Cluster-Token}
  ${p:environment/Cluster-URL}
  ${p:environment/Target-Namespace}
  ~~~
